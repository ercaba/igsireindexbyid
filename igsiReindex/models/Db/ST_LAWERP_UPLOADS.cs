﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class ST_LAWERP_UPLOADS
    {
        [Key]
        public int UPLOADID { get; set; }
        public string OBJECTTYPE { get; set; }
        public int? OBJECTID { get; set; }
        public string FILEPATH { get; set; }
        public string FILENAME { get; set; }
        public DateTime DATEADDED { get; set; }
        public string ADDEDBY { get; set; }
        public int? ADDEDBYUSERID { get; set; }
        public int? ISFOLDER { get; set; }
        public int ISDELETED { get; set; }
        public int ISARCHIEVED { get; set; }
        public string FOLDERNAME { get; set; }

    }
}
