﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices.ComTypes;
using System.Text;
using igsiReindex.models;
using Microsoft.EntityFrameworkCore.Query.Internal;
using Microsoft.EntityFrameworkCore.Query.ResultOperators.Internal;
using Microsoft.EntityFrameworkCore.ValueGeneration.Internal;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using Newtonsoft.Json;
using System.Text.RegularExpressions;
using Elasticsearch.Net;
using Newtonsoft.Json.Linq;
using System.Net.Mail;
using MongoDB.Bson.Serialization.Conventions;

namespace igsiReindex
{
    class MainOperations
    {
        ArrayList idLer = new ArrayList();
        private static IEnumerable<ST_LAWERP_CUSTOMER_SYSTEM_USERS> _userCache = null;
        public static MongoClient client = new MongoClient(MongoUrl.Create("mongodb://192.168.1.160:27017"));
        public static IMongoDatabase db = client.GetDatabase("gsiocr");
        public static IMongoCollection<BsonDocument> ocrCollection = db.GetCollection<BsonDocument>("elasticOcr");
        public static IMongoCollection<BsonDocument> renameCollection = db.GetCollection<BsonDocument>("renameQuue");
        

        public static Regex regex = new Regex(@"  +");
        public static Regex regexPath = new Regex(@"(?=(\\*.?))( )(?=\\)");
        

        public Dictionary<int,string> GetElasticOcrIdDocument()
        {
            Dictionary<int, string> Idler = new Dictionary<int,string>();
            Console.WriteLine("Sorunlu Dökümanları Bulunuyor.");
            var builder = Builders<BsonDocument>.Filter;
            var filt = builder.Eq("Status", 3) & builder.Eq("index", "gsiclient") & builder.Not(builder.Eq("DocType", "adv_dava"));
            // var filter = Builders<BsonDocument>.Filter.Eq("Status", -5) , Builders<BsonDocument>.Filter.Eq("Status", -1);
            List<BsonDocument> result = ocrCollection.Find(filt).ToList();
           

            Console.WriteLine("Sorunlu Dökümanlar Bulundu ve Liste Oluşturuldu.");
            for (int i = 0; i < result.Count(); i++)
            {
                try { 
                if(result[i].GetValue("filePath").ToString()!=null)
                {
                    int AlinanId = result[i].GetValue("igsiId").AsInt32;
                    string filePath = result[i].GetValue("filePath").ToString();
                    Idler.Add(AlinanId, filePath);
                    Console.WriteLine("Kayıt var");
                    //file path olmayan 3 durumları var o yüzden böyle bir kontrol koyduk
                }
                }
                catch
                {
                    Console.WriteLine("İstenilen Özellik bulunamadı.Aranan field veritabanında yok");
                }

            }
            return Idler;

            //Sorunlu Idleri ve filePath Çeker.
        }
        public Dictionary<int,string> GetElasticOcrIdCase()
        {
            Dictionary<int, string> Idler = new Dictionary<int,string>();
            Console.WriteLine("Sorunlu Dökümanları Bulunuyor.");
            var builder = Builders<BsonDocument>.Filter;
            var filt = builder.Eq("Status", 3) & builder.Eq("index", "gsiclient") & builder.Eq("DocType", "adv_dava");
           // var filter = Builders<BsonDocument>.Filter.Eq("Status", -5) , Builders<BsonDocument>.Filter.Eq("Status", -1);
            List<BsonDocument> result = ocrCollection.Find(filt).ToList();
           

            Console.WriteLine("Sorunlu Dökümanlar Bulundu ve Liste Oluşturuldu.");
            for (int i = 0; i < result.Count(); i++)
            {
                try { 
                if(result[i].GetValue("filePath").ToString()!=null)
                {
                    int AlinanId = result[i].GetValue("igsiId").AsInt32;
                    string filePath = result[i].GetValue("filePath").ToString();
                    Idler.Add(AlinanId, filePath);
                    Console.WriteLine("Kayıt var");
                    //file path olmayan 3 durumları var o yüzden böyle bir kontrol koyduk
                }
                }
                catch
                {
                    Console.WriteLine("İstenilen Özellik bulunamadı.Aranan field veritabanında yok");
                }

            }
            return Idler;

            //Sorunlu Idleri ve filePath Çeker.
        }

        public void SorunluIdGuncelle(int Id)
        {
            var filter = Builders<BsonDocument>.Filter.Eq("igsiId", Id);
            var update = Builders<BsonDocument>.Update.Set("Status", -1);
            var result = ocrCollection.UpdateOne(filter, update);
            //-1 sıraya girsin -5 hala atılma süresinde file yok gibi düşünülebilir.

        }

        public int DocumenetStatusCopyAndUpdate(DocumentRaw document)
        {
            //documente ulas oradan yeniden yazdır
            DocumentJson preparedDocument = new DocumentJson();
            string pathPrefix = "";
            string docType = "";
            switch (document.projectType)
            {
                case "Projects":
                    if (document.filePath.StartsWith("mail"))
                    {
                        document.filePath = document.filePath.Replace("mail:", "V:\\MailAttachmentsI\\");
                        pathPrefix = "";
                        docType = "belge";
                        break;
                    }
                    pathPrefix = "\\\\192.168.1.200\\e$\\wwwroot\\LawErp\\ProjectsUploads\\";
                    docType = document.isDataRoom == true ? "dataroom" : "belge";
                    break;
                default:
                    pathPrefix = "\\\\192.168.1.201\\c$\\wwwroot\\LawErp\\TETB\\www\\";
                    docType = "dava";
                    break;
            }
            if (document.projectType != "Projects")
            {
                document.filePath = FixFilePathForCase(document.filePath);
            }

            preparedDocument.DocName = "multipage_adv_" + docType + "_" + document.fileID;
            string filePath = regexPath.Replace(document.filePath, "");
            preparedDocument.fileName = Path.GetFileName(filePath);
            preparedDocument.DocType = "adv_" + docType;
            preparedDocument.igsiId = document.fileID;
            preparedDocument.filePath = pathPrefix + filePath;

            string file = ToFileBinary2(pathPrefix + filePath, preparedDocument);
            if (file == "yok")
            {
                //mail attır
                Console.WriteLine("Doküman bulunamadı ve kopyalanamadı .Status değeri değiştirilmedi.");
                //yok dosya hala igsi üzerinde atılmaya devam ediyor
                //
                return -1;
            }
            return 1;
        }


        public string ToFileBinary2(string filePath, DocumentJson documentJson)
        {
            string file = "yok";
            Console.WriteLine(filePath);
            if (File.Exists(filePath) || filePath == "email")
            {
                file = "var";
                if (filePath != "email")
                {
                    string extension = Path.GetExtension(documentJson.fileName);
                    File.Copy(filePath, "O:\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + extension, true);
                    SorunluIdGuncelle(documentJson.igsiId);
                    Console.WriteLine("Sorunlu Id Guncellendi");
                }
                else
                {
                    //  File.WriteAllText("O:\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + ".email", documentJson.file);
                    Console.WriteLine("Dosya Bulunamadı");
                }
            }
            else
            {
                Console.WriteLine("Dosyaya Ulasılamadı");
            }
            return file;
        }







        public int PrepareDocumentData(DocumentRaw document,bool durum=true)
        {
            //documente ulas oradan yeniden yazdır
            DocumentJson preparedDocument = new DocumentJson();
            var dbOperations = new DbOperations();
            RegexOptions options = RegexOptions.None;
            Regex regex = new Regex("[ ]{2,}", options);

            dynamic userID = document.userID;
            dynamic userFullName = document.user;
            Console.WriteLine(document.fileID);
            Dictionary<string, string> userDetail = GetUserDetails(userID, userFullName);

            string pathPrefix = "";
            string docType = "";
            LastId(document.fileID, document.projectType);
            switch (document.projectType)
            {
                case "Projects":
                    if (document.filePath.StartsWith("mail"))
                    {
                        document.filePath = document.filePath.Replace("mail:", "V:\\MailAttachmentsI\\");
                        pathPrefix = "";
                        docType = "belge";

                        break;
                    }
                    pathPrefix = "\\\\192.168.1.200\\e$\\wwwroot\\LawErp\\ProjectsUploads\\";
                    docType = document.isDataRoom == true ? "dataroom" : "belge";
                    break;
                default:
                    pathPrefix = "\\\\192.168.1.201\\c$\\wwwroot\\LawErp\\TETB\\www\\";
                    docType = "dava";
                    break;
            }
            if (document.projectType != "Projects")
            {
                document.filePath = FixFilePathForCase(document.filePath);
            }

            preparedDocument.DocName = "multipage_adv_" + docType + "_" + document.fileID;
            string filePath = regexPath.Replace(document.filePath, "");
 
            preparedDocument.index = "gsiclient";
            preparedDocument.type = "documents";
            preparedDocument.fileName = Path.GetFileName(filePath);
            preparedDocument.DocType = "adv_" + docType;
            preparedDocument.Category = "";
            preparedDocument.CommitDesc = document.fileName;
            preparedDocument.directoryFilter = dbOperations.GetParents(document.fileID) + " igsi_" + document.projectID + " igsi_" + document.clientID;
            preparedDocument.directoryFilter = regex.Replace(preparedDocument.directoryFilter, " ");
            preparedDocument.Date = ToUnixTime(document.date);
            preparedDocument.igsiId = document.fileID;
            preparedDocument.Extra = "";
            preparedDocument.Tags = "";
            preparedDocument.filePath = pathPrefix + filePath;
            preparedDocument.language = "nolang";
            preparedDocument.Matter = document.projectName;
            preparedDocument.RepoName = document.clientName + "/" + document.projectName;
            preparedDocument.User = userDetail["userName"];
            preparedDocument.displayName = userDetail["fullName"];
            preparedDocument.Versions = "";
            preparedDocument.matterId = document.projectID;
            preparedDocument.clientName = document.clientName;
            preparedDocument.clientNameId = document.clientID;
            preparedDocument.file = "";
            preparedDocument.idField = preparedDocument.DocName;
            preparedDocument.archive = document.archive;
            string file = ToFileBinary(pathPrefix + filePath, preparedDocument);
           // preparedDocument.file = file;
            Console.WriteLine(file);
            //ToMongo(ocrCollection, preparedDocument, -5);

            //dosya eğer SorunluIdleri Düzelt kısmından gelirse file var olarak gelir.
            if (file == "yok")
            {
                //mail attır
                preparedDocument.file = file;
                ToMongo(ocrCollection, preparedDocument, 3);
                //yok dosya hala igsi üzerinde atılmaya devam ediyor
                //
                return -1;
            }


            if(durum==true)
            { 
                ToMongo(ocrCollection, preparedDocument, -1);
            }

            return 1;
        }
      

        public void Gonder(String Icerik="", String Konu="")
        {
            MailMessage ePosta = new MailMessage();
            ePosta.From = new MailAddress("eraycan93@gmail.com");
            //
            ePosta.To.Add("eraycan93@gmail.com");

            //ePosta.Attachments.Add(new Attachment(@"C:\deneme.txt"));
            //
            ePosta.Subject = Konu;
            //
            ePosta.Body = Icerik;
            //
            SmtpClient smtp = new SmtpClient();
            //
            smtp.Credentials = new System.Net.NetworkCredential("eraycan93@gmail.com", "--");
            smtp.Port = 587;
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = true;
            object userState = ePosta;

            try
            {
                smtp.SendAsync(ePosta, (object)ePosta);
            }
            catch (SmtpException ex)
            {
                Console.WriteLine("Hata Var", ex);
            }
            Console.WriteLine("Mail Gönderildi");
        }

        public int PrepareMailData(MailRaw mail, bool notRename = true)
        {
            DocumentJson preparedDocument = new DocumentJson();
            Dictionary<string, string> userDetail = GetUserDetailsMail(mail.FROMMAILADDRESS);
            string pathPrefix = "";
            string docType = "email";
            if (notRename)
            {
                LastId(mail.MAILID, "MAILS");
            }

            Console.WriteLine(mail.MAILID);
            preparedDocument.DocName = "multipage_adv_" + docType + "_" + mail.MAILID;

            //string file = GetFileBase64(pathPrefix + filePath);

            preparedDocument.index = "gsiclientnew";
            preparedDocument.type = "documents";
            preparedDocument.fileName = mail.SUBJECT + ".email";
            preparedDocument.DocType = "adv_" + docType;
            preparedDocument.Category = "";
            preparedDocument.CommitDesc = mail.SUBJECT;
            preparedDocument.Date = ToUnixTime((DateTime)mail.DATASENT);
            preparedDocument.igsiId = mail.MAILID;
            preparedDocument.Extra = "";
            preparedDocument.Tags = "";
            preparedDocument.language = "nolang";
            preparedDocument.Matter = mail.projectName;
            preparedDocument.RepoName = mail.clientName + "/" + mail.projectName;
            preparedDocument.User = userDetail["userName"];
            preparedDocument.Versions = "";
            preparedDocument.matterId = (int)mail.PROJECTID;
            preparedDocument.clientName = mail.clientName;
            preparedDocument.clientNameId = (int)mail.clientID;
            preparedDocument.file = (mail.BODY);
            preparedDocument.fromMail = mail.FROMMAILADDRESS;
            preparedDocument.displayName = mail.DISPLAYNAME;
            var peopleList = GetEmailsPeople(mail.MAILID);
            preparedDocument.to = peopleList["toList"];
            preparedDocument.ccList = peopleList["ccList"];
            preparedDocument.directoryFilter = "igsi_" + mail.PROJECTID + " igsi_" + mail.clientID;
            preparedDocument.idField = preparedDocument.DocName;
            string file = ToFileBinary("email", preparedDocument);

            //   var fileStatus = this.ToFile(preparedDocument);

            ToMongo(ocrCollection, preparedDocument, -1);
            return 1;
        }

        public int renameMail(MailRaw mail)
        {

            PrepareMailData(mail, false);
            return 1;
        }
        public string renameElasticSearch(DocumentRaw document)
        {
            //if (document.ISFOLDER == 1 && document.REPORTTYPEID == 10)
            //{
            //    return "";
            //}
            var dbOperations = new DbOperations();

            Console.WriteLine(document.fileID);

            string docType = "";
            switch (document.projectType)
            {
                case "Projects":
                    docType = document.isDataRoom == true ? "dataroom" : "belge";
                    break;
                case "email":
                    docType = "email";
                    break;
                default:
                    docType = "dava";
                    break;
            }
            if (document.ISFOLDER == 0)
            {
                DocumentJson preparedDocument = new DocumentJson();
                preparedDocument.DocName = "multipage_adv_" + docType + "_" + document.fileID;
                preparedDocument.idField = Convert.ToString(document.fileID);
                preparedDocument.Matter = document.projectName;
                preparedDocument.RepoName = document.clientName + "/" + document.projectName;
                preparedDocument.matterId = document.projectID;
                preparedDocument.clientName = document.clientName;
                preparedDocument.clientNameId = document.clientID;
                preparedDocument.fileName = document.fileName;
                preparedDocument.Category = "";
                preparedDocument.RenameCondutation = "Rename";
                preparedDocument.DocType = docType == "email" ? "email" : "Documents";
                preparedDocument.RenameType = "Documents";
                if (docType != "email")
                {
                    LastDateRenames(document.LOGID);
                    preparedDocument.filePath = dbOperations.GetFullPath(document.fileID) + preparedDocument.fileName; ;
                    preparedDocument.newDirectoryFilter = dbOperations.GetParents(document.fileID) + " igsi_" + document.projectID + " igsi_" + document.clientID + " igsi_" + docType + "_" + document.projectID + " igsi_" + docType + "_" + document.clientID;
                }
                else
                {
                    LastDateMailsRenames(document.LOGID);
                    preparedDocument.newDirectoryFilter = "igsi_" + document.projectID + " igsi_" + document.clientID + " igsi_" + docType + "_" + document.projectID + " igsi_" + docType + "_" + document.clientID;
                }

                preparedDocument.newDirectoryFilter = regex.Replace(preparedDocument.newDirectoryFilter, " ");
                ToMongo(renameCollection, preparedDocument, -1);
            }
            else
            {
                DocumentJson preparedDocument = new DocumentJson();
                preparedDocument.DocName = "multipage_adv_" + docType + "_" + document.fileID;
                preparedDocument.idField = Convert.ToString(document.fileID);
                preparedDocument.Matter = document.projectName;
                preparedDocument.RepoName = document.clientName + "/" + document.projectName;
                preparedDocument.matterId = document.projectID;
                preparedDocument.clientName = document.clientName;
                preparedDocument.clientNameId = document.clientID;
                preparedDocument.fileName = document.FOLDERNAME;
                preparedDocument.Category = "";
                preparedDocument.RenameCondutation = "Rename";
                preparedDocument.RenameType = "Folder";
                preparedDocument.filePath = dbOperations.GetFullPath(document.fileID);

                preparedDocument.newDirectoryFilter = dbOperations.GetParents(document.fileID) + " igsi_" + document.projectID + " igsi_" + document.clientID + " igsi_" + docType + "_" + document.projectID + " igsi_" + docType + "_" + document.clientID;
                preparedDocument.newDirectoryFilter = regex.Replace(preparedDocument.newDirectoryFilter, " ");
                if (document.ISFOLDER == 1 && document.REPORTTYPEID == 10)
                {
                    preparedDocument.oldDirectoryFilter = preparedDocument.newDirectoryFilter;
                }
                else
                {
                    preparedDocument.oldDirectoryFilter = dbOperations.GetOldParents(document.fileID) + " igsi_" + document.projectID + " igsi_" + document.clientID + " igsi_" + docType + "_" + document.projectID + " igsi_" + docType + "_" + document.clientID;
                    preparedDocument.oldDirectoryFilter = regex.Replace(preparedDocument.oldDirectoryFilter, " ");

                }
                ToMongo(renameCollection, preparedDocument, -1);

            }

            return "";
        }

        public string GetFileBase64(string path)
        {
            string file = "yok";

            if (File.Exists(path))
            {
                file = "var";

                Byte[] bytes = File.ReadAllBytes(path);
                file = Convert.ToBase64String(bytes).ToJson();
            }
            return file;

        }
        public string StringTurkishToEnglishFix(string text)
        {
            text = text.Replace("İ", "I");
            text = text.Replace("ı", "i");
            text = text.Replace("Ğ", "G");
            text = text.Replace("ğ", "g");
            text = text.Replace("Ö", "O");
            text = text.Replace("ö", "o");
            text = text.Replace("Ü", "U");
            text = text.Replace("ü", "u");
            text = text.Replace("Ş", "S");
            text = text.Replace("ş", "s");
            text = text.Replace("Ç", "C");
            text = text.Replace("ç", "c");
            text = text.Replace(" ", "_");
            text = text.Replace(" ", "_");
            return text;
        }


        public void LastId(int lastID, string projectType)
        {
            var idFile = "";
            if (projectType == "Projects")
            {
                idFile = "\\lastIdProjects.txt";
            }
            else if (projectType == "MAILS") idFile = "\\lastIdMails.txt";
            else idFile = "\\lastIdCases.txt";
            string path = Directory.GetCurrentDirectory() + idFile;
            using (var writer = File.CreateText(path))
            {
                writer.WriteLine(lastID);
            }
        }
        public void LastDateRenames(int lastID)
        {
            var dateFile = "";
            dateFile = "\\lastDateRenames.txt";
            string path = Directory.GetCurrentDirectory() + dateFile;
            using (var writer = File.CreateText(path))
            {
                writer.WriteLine(lastID);
            }
        }
        public void LastDateMailsRenames(int lastID)
        {
            var dateFile = "";
            dateFile = "\\lastDateMails.txt";
            string path = Directory.GetCurrentDirectory() + dateFile;
            using (var writer = File.CreateText(path))
            {
                writer.WriteLine(lastID);
            }
        }
        public string FixFilePathForCase(string path)
        {
            string fixedPath = path.Replace("../../", "");
            fixedPath = fixedPath.Replace("/", "\\");
            return fixedPath;
        }

        public static long ToUnixTime(DateTime date)
        {
            var epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            return Convert.ToInt64((date - epoch).TotalSeconds);
        }

        public Dictionary<string, string> GetUserDetails(dynamic userID, dynamic userFullName)
        {
            if (userFullName == null)
            {
                userFullName = "";
            }

            var values = new Dictionary<string, string>();

            if (userID == null || userID.ToString() == "" || userID == 0)
            {
                values.Add("userName", "igsisystem");
                values.Add("fullName", "IGSISystem");
                return values;
            }

            if (_userCache == null)
            {
                var dbOperations = new DbOperations();
                _userCache = dbOperations.GetUserNames();
            }
            var userDetail = _userCache.FirstOrDefault(user => user.USERID == userID);
            if (userDetail == null)
            {
                string userFullNameString = userFullName.ToString();
                values.Add("userName", StringTurkishToEnglishFix(userFullNameString).ToLower());
                values.Add("fullName", userFullName);
                return values;
            }
            values.Add("userName", (userDetail.USERNAME));
            values.Add("fullName", (userDetail.FIRSTNAME + " " + userDetail.LASTNAME));
            return values;
        }

        public Dictionary<string, string> GetUserDetailsMail(dynamic fromMail)
        {
            var values = new Dictionary<string, string>();

            if (fromMail == null || fromMail.ToString() == "")
            {
                values.Add("userName", "igsisytem");
                values.Add("fullName", "IGSISystem");
                return values;
            }

            if (_userCache == null)
            {
                var dbOperations = new DbOperations();
                _userCache = dbOperations.GetUserNames();
            }

            var userDetail = _userCache.FirstOrDefault(user => user.EMAIL == fromMail);
            if (userDetail == null)
            {
                values.Add("userName", fromMail);
                return values;
            }
            values.Add("userName", (userDetail.USERNAME));
            return values;
        }

        public int ToMongo(dynamic collection, DocumentJson documentJson, int Status)
        {
            //mongoya yazdığı yer
            documentJson.index = "gsiclient";
            documentJson.RDate = DateTime.Now;
            documentJson.file = "";
            documentJson.Status = Status;
            
            ConventionRegistry.Register("IgnoreIfDefault",
                            new ConventionPack { new IgnoreIfDefaultConvention(true) },
                            t => true);
            collection.InsertOne(documentJson.ToBsonDocument());
            return 1;
        }

        public Dictionary<string, string> GetEmailsPeople(int mailId)
        {
            var dbOperations = new DbOperations();
            var peopleList = dbOperations.GetEmailsPeople(mailId);
            string toList = "";
            string ccList = "";
            foreach (var people in peopleList)
            {
                if (people.TYPEID == 1)
                {
                    toList += (people.MAILADDRESS);
                    toList += ",";
                }
                else
                {
                    ccList += (people.MAILADDRESS);
                    ccList += ",";
                }

            }

            var values = new Dictionary<string, string>();
            values.Add("toList", (toList.TrimEnd(',')));
            values.Add("ccList", (ccList.TrimEnd(',')));

            return values;
        }

        public string ToFileBinary(string filePath, DocumentJson documentJson)
        {
            string file = "yok";
            Console.WriteLine(filePath);
            if (File.Exists(filePath) || filePath == "email")
            {
                file = "var";
                if (filePath != "email")
                {
                    string extension = Path.GetExtension(documentJson.fileName);
                   // Byte[] bytes = File.ReadAllBytes(filePath);
                  
                     File.Copy(filePath, "O:\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + extension,true);
                    

                    //File.WriteAllBytes("C:\\serviceDeneme\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + extension, bytes);
                    //File.WriteAllBytes("O:\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + extension, bytes);
                    //Burası Sorunlu Kısmı Çözüyor . filePath ile documentJson Yerlerini tutmak gerekiyor
                    //Readallbyte olarak metin okunur burası normalde O:\\
                    //write ile belirnen pathe yazılır.
                }
                else
                {
                    //  File.WriteAllText("O:\\" + "multipage_" + documentJson.DocType + "_" + documentJson.igsiId + ".email", documentJson.file);
                }
            }
            return file;
        }
    

    }
}
