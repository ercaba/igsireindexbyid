﻿using System;
using System.Collections.Generic;
using System.Text;
using igsiReindex.models;
using Microsoft.EntityFrameworkCore;

namespace igsiReindex
{
    class DocumenTDbContext : Microsoft.EntityFrameworkCore.DbContext
    {


        //protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        //{
        //    optionsBuilder.UseSqlServer("data source=localhost;initial catalog=TimeBilling;Trusted_Connection=True;");
        //}
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("data source=192.168.1.8;initial catalog=TimeBilling;user id=sql2008user;password=sql2008user@GSI;Connection Timeout=600;MultipleActiveResultSets=true", options => options.EnableRetryOnFailure());
        }

        public DbSet<ST_LAWERP_UPLOADS> ST_LAWERP_UPLOADS { get; set; }
        public DbSet<ST_LAWERP_CASES> ST_LAWERP_CASES { get; set; }
        public DbSet<Clients> Clients { get; set; }
        public DbSet<Projects> Projects { get; set; }
        public DbSet<ST_LAWERP_CUSTOMER_SYSTEM_USERS> ST_LAWERP_CUSTOMER_SYSTEM_USERS { get; set; }
        public DbSet<MT_LAWERP_DATA_ROOM_PROJECTS> MT_LAWERP_DATA_ROOM_PROJECTS { get; set; }
        public DbSet<ST_LAWERP_CUSTOMERSYSTEM_MAILS> ST_LAWERP_CUSTOMERSYSTEM_MAILS { get; set; }
        public DbSet<MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE> MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE { get; set; }
        public DbSet<ST_LAWERP_CUSTOMER_SYSTEM_LOGS> ST_LAWERP_CUSTOMER_SYSTEM_LOGS { get; set; }
        public DbSet<DirectoryFilter> DirectoryFilter { get; set; }
        public DbSet<FullPath> FullPath { get; set; }
        public DbSet<AC_OLD_DIRECTORY_FILTER_T> AC_OLD_DIRECTORY_FILTER_T { get; set; }

    }
}
