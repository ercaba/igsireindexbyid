﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class AC_OLD_DIRECTORY_FILTER_T
    {

        [Key]
        public int ID { get; set; }
        public int UPLOADID { get; set; }
        public string directoryFilter { get; set; }
    }
}
