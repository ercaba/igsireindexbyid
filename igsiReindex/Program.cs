﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Linq.Expressions;
using igsiReindex.models;
using Microsoft.EntityFrameworkCore.Query.Internal;
using System.Threading;
using Newtonsoft.Json;
using System.Globalization;
using Elasticsearch.Net;


/*
 * Bu program genel olarak mongo db ye mongo::gsiocr->elasticOcr ve mongo::gsiocr->renameQuue collectionlarına datayı yazmak. Aynı zamanda dosyayı /ocrPool a fiziksel dosyaları aktarmakdır.
 * 
 * ONEMLİ NOT: /ocrPool diski doldur boşalt sistemi ile çalışmakatadır. Herhengi bir sorun olma durumu için ahmet can otomatik silme yapmamaış. Dolayısıyla burası kontrol edililmelidir.
 * /ocrPool altında 2 klasor var. biri igsi diğeri mecelle. BUnların içindekiler aslında temp dosyalarıdır.
 * 
 * silmek için; find . -mtime +30 -type f -delete konutu kullan. +30 un anlamı 30 gün ve üzerindeki date li dosyaları sil.
 *  
 *
 * 
 * 
 */

namespace igsiReindex
{
    class Program
    {
        static void Main(string[] args)
        {
            var cultureInfo = new CultureInfo("tr-TR");
            CultureInfo.DefaultThreadCurrentCulture = cultureInfo;
            CultureInfo.DefaultThreadCurrentUICulture = cultureInfo;

           MongoManuelWrite();
        
        
           
           //sadece yazdirmak işlemi için
            
        }

        public static string MongoManuelWrite()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            var dbOperations = new DbOperations();
            var mainOperations = new MainOperations();
            Console.WriteLine("İd Değerleri okunuyor");
            string path = Directory.GetCurrentDirectory() + "\\ids.txt";

            string idlertxt = System.IO.File.ReadAllText(path).ToString();
            string[] sorunluIdler = idlertxt.Split(',');
            foreach (var sId in sorunluIdler)
            {
                var preparedDocument = dbOperations.GetProjectProblemFilesNotCase(Convert.ToInt32(sId));
                foreach (var document in preparedDocument)
                {
                    var preparedDocument2 = mainOperations.PrepareDocumentData(document);
                    Console.WriteLine("ID:{0} Document FileName:{1}", sId, document.fileName);
                }

            }

            return "Manuel yazdırmak işlemi bitti";
        }

        
    }
}