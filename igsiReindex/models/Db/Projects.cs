﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace igsiReindex.models
{
    class Projects
    {
        [Key]
        public int ProjectID { get; set; }
        public string ProjectName { get; set; }
        public int ClientID { get; set; }
        public bool ISDATAROOM { get; set; }
        [Column("Active/Passive")]
        public int ActivePassive { get; set; }

    }
}
