﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class ST_LAWERP_CUSTOMER_SYSTEM_LOGS
    {

        [Key]
        public Int64 LOGID { get; set; }

        public Int16 REPORTTYPEID { get; set; }
        public int OBJECTID { get; set; }
        public int OPERANTUSERID { get; set; }
        public DateTime LOGDATE { get; set; }


    }
}


