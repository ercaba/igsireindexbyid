﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class ST_LAWERP_CUSTOMERSYSTEM_MAILS
    {
        [Key]
        public int MAILID { get; set; }
        public string FROMMAILADDRESS { get; set; }
        public string DISPLAYNAME { get; set; }
        public string SUBJECT { get; set; }
        public DateTime DATASENT { get; set; }
        public int PROJECTID { get; set; }
        public string BODY { get; set; }
        public bool ISFORONLYINTERNAL { get; set; }
        public string GUID { get; set; }
        public bool HASATTACHMENTEXCEEDINGLIMIT { get; set; }
        public bool HASATTACHMENT { get; set; }
    }
}
