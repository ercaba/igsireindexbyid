﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class FullPath
    {
        [Key]
        public string fullPath { get; set; }
    }
}
