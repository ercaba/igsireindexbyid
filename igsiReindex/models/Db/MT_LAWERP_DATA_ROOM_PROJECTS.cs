﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class MT_LAWERP_DATA_ROOM_PROJECTS
    {
        [Key]
        public int DATAROOMID { get; set; }
        public int PROJECTID { get; set; }
        public bool ISDATAROOM { get; set; }
    }
}
