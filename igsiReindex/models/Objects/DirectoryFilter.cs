﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class DirectoryFilter
    {

        [Key]
        public int ID { get; set; }
        public string FOLDERID { get; set; }
    }
}
