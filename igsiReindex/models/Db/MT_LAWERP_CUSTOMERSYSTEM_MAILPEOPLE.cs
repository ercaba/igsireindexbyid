﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE
    {
        [Key]
        public int MAILID { get; set; }
        public int TYPEID { get; set; }
        public string DISPLAYNAME { get; set; }
        public string MAILADDRESS { get; set; }
    }
}
