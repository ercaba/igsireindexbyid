﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection.Metadata;
using System.Text;
using igsiReindex.models;
using Microsoft.EntityFrameworkCore;

namespace igsiReindex
{

    class DbOperations
    {
        //public list
        public IQueryable<DocumentRaw> GetProjectFiles()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();

            string path = Directory.GetCurrentDirectory() + "\\lastIdProjects.txt";

            int lastId = Convert.ToInt32(System.IO.File.ReadAllText(path));
            var document = from documents in ctx.ST_LAWERP_UPLOADS.AsNoTracking()
                           join projects in ctx.Projects on documents.OBJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           join dataroom in ctx.MT_LAWERP_DATA_ROOM_PROJECTS on projects.ProjectID equals dataroom.PROJECTID into dataroomjoin
                           from dataroom in dataroomjoin.DefaultIfEmpty()
                           where documents.UPLOADID > lastId && documents.OBJECTTYPE == "Projects" && documents.ISFOLDER != 1 && documents.ISDELETED == 0

                           orderby documents.UPLOADID
                           select new DocumentRaw { fileID = documents.UPLOADID, fileName = documents.FILENAME, filePath = documents.FILEPATH, user = documents.ADDEDBY, userID = documents.ADDEDBYUSERID, date = (documents == null || documents.DATEADDED == null) ? DateTime.Parse("19-12-1994") : documents.DATEADDED, projectType = documents.OBJECTTYPE, projectID = projects.ProjectID, projectStatus = projects.ActivePassive, projectName = projects.ProjectName, isDataRoom = (dataroom == null || dataroom.ISDATAROOM == null || dataroom.ISDATAROOM == false) ? false : true, clientName = clients.CompanyName, clientID = clients.ClientID, archive = documents.ISARCHIEVED };
            return document;
        }
     
        public IQueryable<DocumentRaw> GetProjectProblemFilesNotCase(int gelenId)
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            var document = from documents in ctx.ST_LAWERP_UPLOADS.AsNoTracking()
                           join projects in ctx.Projects on documents.OBJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           join dataroom in ctx.MT_LAWERP_DATA_ROOM_PROJECTS on projects.ProjectID equals dataroom.PROJECTID into dataroomjoin
                           from dataroom in dataroomjoin.DefaultIfEmpty()
                           where documents.UPLOADID == gelenId && documents.ISFOLDER != 1 && documents.ISDELETED == 0 
                           orderby documents.UPLOADID
                           select new DocumentRaw { fileID = documents.UPLOADID, fileName = documents.FILENAME, filePath = documents.FILEPATH, user = documents.ADDEDBY, userID = documents.ADDEDBYUSERID, date = (documents == null || documents.DATEADDED == null) ? DateTime.Parse("19-12-1994") : documents.DATEADDED, projectType = documents.OBJECTTYPE, projectID = projects.ProjectID, projectStatus = projects.ActivePassive, projectName = projects.ProjectName, isDataRoom = (dataroom == null || dataroom.ISDATAROOM == null || dataroom.ISDATAROOM == false) ? false : true, clientName = clients.CompanyName, clientID = clients.ClientID, archive = documents.ISARCHIEVED };

            return document;
        }

        public IQueryable<DocumentRaw> GetProjectProblemFilesCase(int gelenId)
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            var document = from documents in ctx.ST_LAWERP_UPLOADS.AsNoTracking()
                           join cases in ctx.ST_LAWERP_CASES on documents.OBJECTID equals cases.CASEID
                           join projects in ctx.Projects on cases.PROJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           where documents.OBJECTTYPE != "Projects" && documents.ISFOLDER != 1 && documents.ISDELETED == 0
                                   && documents.UPLOADID == gelenId
                           orderby documents.UPLOADID
                           select new DocumentRaw { fileID = documents.UPLOADID, fileName = documents.FILENAME, filePath = documents.FILEPATH, user = documents.ADDEDBY, userID = documents.ADDEDBYUSERID, date = (documents == null || documents.DATEADDED == null) ? DateTime.Parse("19-12-1994") : documents.DATEADDED, projectType = documents.OBJECTTYPE, projectID = projects.ProjectID, projectName = projects.ProjectName, clientName = clients.CompanyName, clientID = clients.ClientID };

            return document;
        }



        public IQueryable<MailRaw> GetEmails()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();

            string path = Directory.GetCurrentDirectory() + "\\lastIdMails.txt";

            int lastId = Convert.ToInt32(System.IO.File.ReadAllText(path));


            var mail = from mails in ctx.ST_LAWERP_CUSTOMERSYSTEM_MAILS.AsNoTracking()
                       join projects in ctx.Projects on mails.PROJECTID equals projects.ProjectID
                       join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                       where mails.MAILID > lastId
                       orderby mails.MAILID
                       select new MailRaw() { MAILID = mails.MAILID, SUBJECT = mails.SUBJECT, DATASENT = (mails == null || mails.DATASENT == null) ? DateTime.Parse("19-12-1994") : mails.DATASENT, PROJECTID = mails.PROJECTID, DISPLAYNAME = mails.DISPLAYNAME, FROMMAILADDRESS = mails.FROMMAILADDRESS, BODY = mails.BODY, projectName = projects.ProjectName, clientName = clients.CompanyName, clientID = clients.ClientID };

            return mail;
        }

        public IQueryable<MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE> GetEmailsPeople(int mailId)
        {
            DocumenTDbContext ctx = new DocumenTDbContext();

            var mailsPeopleList = from mailsPeople in ctx.MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE.AsNoTracking()
                                  where mailsPeople.MAILID == mailId
                                  select new MT_LAWERP_CUSTOMERSYSTEM_MAILPEOPLE() { MAILID = mailsPeople.MAILID, DISPLAYNAME = mailsPeople.DISPLAYNAME, TYPEID = mailsPeople.TYPEID, MAILADDRESS = mailsPeople.MAILADDRESS };



            return mailsPeopleList;
        }

        public IQueryable<MailRaw> GetEmail(int emailID)
        {
            DocumenTDbContext ctx = new DocumenTDbContext();

            string path = Directory.GetCurrentDirectory() + "\\lastIdMails.txt";



            var mail = from mails in ctx.ST_LAWERP_CUSTOMERSYSTEM_MAILS.AsNoTracking()
                       join projects in ctx.Projects on mails.PROJECTID equals projects.ProjectID
                       join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                       where mails.MAILID == emailID
                       orderby mails.MAILID
                       select new MailRaw() { MAILID = mails.MAILID, SUBJECT = mails.SUBJECT, DATASENT = (mails == null || mails.DATASENT == null) ? DateTime.Parse("19-12-1994") : mails.DATASENT, PROJECTID = mails.PROJECTID, DISPLAYNAME = mails.DISPLAYNAME, FROMMAILADDRESS = mails.FROMMAILADDRESS, BODY = mails.BODY, projectName = projects.ProjectName, clientName = clients.CompanyName, clientID = clients.ClientID };

            return mail;
        }

        public IQueryable<DocumentRaw> GetCaseFiles()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            string path = Directory.GetCurrentDirectory() + "\\lastIdCases.txt";

            int lastId = Convert.ToInt32(System.IO.File.ReadAllText(path));
            var document = from documents in ctx.ST_LAWERP_UPLOADS.AsNoTracking()
                           join cases in ctx.ST_LAWERP_CASES on documents.OBJECTID equals cases.CASEID
                           join projects in ctx.Projects on cases.PROJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           where documents.OBJECTTYPE != "Projects" && documents.ISFOLDER != 1 && documents.ISDELETED == 0
                                   && documents.UPLOADID > lastId
                           orderby documents.UPLOADID
                           select new DocumentRaw { fileID = documents.UPLOADID, fileName = documents.FILENAME, filePath = documents.FILEPATH, user = documents.ADDEDBY, userID = documents.ADDEDBYUSERID, date = (documents == null || documents.DATEADDED == null) ? DateTime.Parse("19-12-1994") : documents.DATEADDED, projectType = documents.OBJECTTYPE, projectID = projects.ProjectID, projectName = projects.ProjectName, clientName = clients.CompanyName, clientID = clients.ClientID };

            return document;
        }

        public IQueryable<DocumentRaw> getRenames()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            string path = Directory.GetCurrentDirectory() + "\\lastDateRenames.txt";


            int lastId = Convert.ToInt32(System.IO.File.ReadAllText(path));

            var document = from logs in ctx.ST_LAWERP_CUSTOMER_SYSTEM_LOGS.AsNoTracking()
                           join documents in ctx.ST_LAWERP_UPLOADS on logs.OBJECTID equals documents.UPLOADID
                           join projects in ctx.Projects on documents.OBJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           join dataroom in ctx.MT_LAWERP_DATA_ROOM_PROJECTS on projects.ProjectID equals dataroom.PROJECTID into dataroomjoin
                           from dataroom in dataroomjoin.DefaultIfEmpty()
                           where logs.LOGID > lastId && (logs.REPORTTYPEID == 63 || logs.REPORTTYPEID == 10)
                           orderby documents.UPLOADID
                           select new DocumentRaw { fileID = documents.UPLOADID, fileName = documents.FILENAME, user = documents.ADDEDBY, userID = logs.OPERANTUSERID, date = logs.LOGDATE, projectType = documents.OBJECTTYPE, ISFOLDER = documents.ISFOLDER, REPORTTYPEID = logs.REPORTTYPEID, isDataRoom = (dataroom == null || dataroom.ISDATAROOM == null || dataroom.ISDATAROOM == false) ? false : true, projectID = projects.ProjectID, clientID = clients.ClientID, LOGID = Convert.ToInt32(logs.LOGID), clientName = clients.CompanyName, projectName = projects.ProjectName, FOLDERNAME = documents.FOLDERNAME };

            return document;
        }

        public IQueryable<DocumentRaw> getEmailMove()
        {
            DocumenTDbContext ctx = new DocumenTDbContext();
            string path = Directory.GetCurrentDirectory() + "\\lastDateMails.txt";
            int lastId = Convert.ToInt32(System.IO.File.ReadAllText(path));

            var document = from logs in ctx.ST_LAWERP_CUSTOMER_SYSTEM_LOGS.AsNoTracking()
                           join mails in ctx.ST_LAWERP_CUSTOMERSYSTEM_MAILS on logs.OBJECTID equals mails.MAILID
                           join projects in ctx.Projects on mails.PROJECTID equals projects.ProjectID
                           join clients in ctx.Clients on projects.ClientID equals clients.ClientID
                           where logs.LOGID > lastId && (logs.REPORTTYPEID == 82)
                           orderby logs.LOGID
                           select new DocumentRaw() { fileID = mails.MAILID, fileName = mails.SUBJECT, projectID = mails.PROJECTID, projectName = projects.ProjectName, clientName = clients.CompanyName, clientID = clients.ClientID, LOGID = Convert.ToInt32(logs.LOGID) , projectType ="email" , ISFOLDER = 0};

            return document;
        }
        public IEnumerable<ST_LAWERP_CUSTOMER_SYSTEM_USERS> GetUserNames()
        {

            DocumenTDbContext ctx = new DocumenTDbContext();

            var users = ctx.ST_LAWERP_CUSTOMER_SYSTEM_USERS;
            return users;
        }

        public string GetParents(int DocumentID)
        {

            DocumenTDbContext ctx = new DocumenTDbContext();

            var parents = ctx.DirectoryFilter.FromSql("SELECT * from GetAncestry({0})", DocumentID).FirstOrDefault();
            return parents.FOLDERID;
        }

        public string GetOldParents(int DocumentID)
        {

            DocumenTDbContext ctx = new DocumenTDbContext();

            var parents = ctx.AC_OLD_DIRECTORY_FILTER_T.FromSql("SELECT TOP (1) * from AC_OLD_DIRECTORY_FILTER_T where UPLOADID = {0} order by ID desc", DocumentID).FirstOrDefault();
            return "igsi_" + parents.UPLOADID + " " + parents.directoryFilter;
        }

        public string GetFullPath(int DocumentID)
        {

            DocumenTDbContext ctx = new DocumenTDbContext();

            var fullPath = ctx.FullPath.FromSql("SELECT * from  [TimeBilling].[dbo].[GetFullPath]({0})", DocumentID).FirstOrDefault();
            return fullPath.fullPath;
        }
    }
}
