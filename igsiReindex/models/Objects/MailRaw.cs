﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class MailRaw
    {

        [Key]
        public int MAILID { get; set; }
        public string FROMMAILADDRESS { get; set; }
        public string DISPLAYNAME { get; set; }
        public string SUBJECT { get; set; }
        public DateTime? DATASENT { get; set; }
        public int? PROJECTID { get; set; }
        public int? clientID { get; set; }
        public string BODY { get; set; }
        public string clientName { get; set; }
        public string projectName { get; set; }
        public int LOGID { get; set; }


    }
}
