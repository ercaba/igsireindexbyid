﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class DocumentJson
    {
        [BsonElement("id")]
        public string idField { get; set; }
        public string index { get; set; }
        public string User { get; set; }
        public string displayName { get; set; }
        public string fileName { get; set; }
        public string directoryFilter { get; set; }
        public string newDirectoryFilter { get; set; }
        public string oldDirectoryFilter { get; set; }
        public string type { get; set; }
        public string Category { get; set; }
        public string CommitDesc { get; set; }
        public int matterId { get; set; }
        public int igsiId { get; set; }
        public int clientNameId { get; set; }
        public string clientName { get; set; }
        public long Date { get; set; }
        public DateTime RDate { get; set; }
        public string DocName { get; set; }
        public string DocType { get; set; }
        public string Extra { get; set; }
        public string filePath { get; set; }
        public string language { get; set; }
        public string Matter { get; set; }
        public string RepoName { get; set; }
        public string Versions { get; set; }
        public string Tags { get; set; }
        public string file { get; set; }
        public int archive { get; set; }
        public string fromMail { get; set; }
        public string to { get; set; }
        public string ccList { get; set; }
        public int Status { get; set; }
        public string RenameCondutation { get; set; }
        public string RenameType { get; set; }

    }
}
