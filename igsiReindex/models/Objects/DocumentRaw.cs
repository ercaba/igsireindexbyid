﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class DocumentRaw
    {

        [Key]
        public int fileID { get; set; }
        public string fileName { get; set; }
        public string filePath { get; set; }
        public string user { get; set; }
        public dynamic userID { get; set; }
        public DateTime date { get; set; }
        public string projectType { get; set; }
        public int projectID { get; set; }
        public string projectName { get; set; }
        public int projectStatus { get; set; }
        public string clientName { get; set; }
        public int clientID { get; set; }
        public bool isDataRoom { get; set; }
        public int? ISFOLDER { get; set; }
        public int REPORTTYPEID { get; set; }
        public string directoryFilter { get; set; }
        public int LOGID { get; set; }

        public int archive { get; set; }
        public string FOLDERNAME { get; set; }


    }
}
