﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class ST_LAWERP_CUSTOMER_SYSTEM_USERS
    {
        [Key]
        public int USERID { get; set; }
        public string USERNAME { get; set; }
        public string FIRSTNAME { get; set; }
        public string LASTNAME { get; set; }
        public string EMAIL { get; set; }

    }
}
