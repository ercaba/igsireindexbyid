﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace igsiReindex.models
{
    class Clients
    {
        [Key]
        public int ClientID { get; set; }
        public string CompanyName { get; set; }
    }
}
